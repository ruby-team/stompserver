stompserver (0.9.9gem-5) unstable; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Update watch file format version to 4.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-daemons,
      ruby-eventmachine and ruby-hoe.
    + stompserver: Drop versioned constraint on lsb-base, ruby-daemons,
      ruby-eventmachine and ruby-hoe in Depends.

  [ Lucas Kanashiro ]
  * d/p/0006-Fix-FTBFS-with-Ruby-3.2.patch: call File.exist? instead of
    File.exists?
  * Declare compliance with Debian Policy 4.6.2.
  * d/control: depend on ${ruby:Depends} instead of ruby-interpreter.
  * d/control: do not depend on lsb-base.

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 21 Feb 2024 19:08:52 -0300

stompserver (0.9.9gem-4) unstable; urgency=medium

  * Team upload.

  [ Jonas Genannt ]
  * updated changelog for release

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields

  [ Sebastien Badia ]
  * Bump debhelper compatibility level to 10
  * Bump Standards-Version to 3.9.8 (no changes needed)
  * Add missing depends on lsb-base (init.d-script-needs-depends-on-lsb-base)
  * Update packaging with a new wrap-and-sort --wrap-always run
    (Closes: #830238)
  * d/control: Fix package section (from ruby, to net)
    (Closes: #832259) Thanks Ben!

 -- Sebastien Badia <sbadia@debian.org>  Wed, 21 Dec 2016 13:09:16 +0100

stompserver (0.9.9gem-3) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields

  [ Jonas Genannt ]
  * d/control:
    - bumped standards version to 3.9.5 (no changes needed)
      - README.source no longer needed
    - changed ruby1.8 dependency to ruby | ruby-interpreter
    - changed Ruby-Versions to all
  * ruby_hashbang.patch: refreshed from upstream, added DEP-3 header
    (changed ruby1.8 into env ruby)
  * added patch for Ruby 1.9/2.0 support from Upstream (Closes: #730880)
  * d/stompserver.init updated init script
    - added DIETIME/STARTTIME (Closes: #576452)
    - updated status to lsb init function
    - added source of default/stompserver (LP: #704687)

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Thu, 16 Jan 2014 14:48:48 +0100

stompserver (0.9.9gem-2) unstable; urgency=low

  * Bumped build-dependency on gem2deb to >= 0.3.0~.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 26 Jun 2012 23:54:44 +0200

stompserver (0.9.9gem-1) unstable; urgency=low

  * Use upstream source from the gem rather than RubyForge.

 -- Paul van Tilburg <paulvt@debian.org>  Sat, 26 May 2012 00:42:45 +0200

stompserver (0.9.9-2) unstable; urgency=low

  * Source packages adapted according to the new Ruby policy:
    - Build for Ruby 1.8 only; there is no support for Ruby 1.9.1.
    - Migrated to pkg-ruby-extras git repos. Changed the Vcs-* fields in
      debian/control accordingly.
    - Changed the depends to follow the new Ruby library naming scheme.
  * debian/control:
    - Added a default DM-Upload-Allowed field set to yes.
    - Standards-Version bumped to 3.9.3; no changes required.
    - Set XS-Ruby-Versions to ruby1.8.
    - Changed the build-depends for using gem2deb instead of ruby-pkg-tools.
    - Switched the maintainer with the uploaders field as per new
      convention the team is the default maintainer.
  * debian/copyright: reworked to fit the Debian copyright format version 1.0.
  * debian/patches: refreshed patches to removed fuzz.
  * debian/watch: updated to use gemwatch instead Rubyforge.
  * debian/stompserver.init
    - Depend on $remote_fs instead of $local_fs.
    - Added a description.

 -- Paul van Tilburg <paulvt@debian.org>  Fri, 25 May 2012 22:57:44 +0200

stompserver (0.9.9-1) unstable; urgency=low

  * Initial release (Closes: #537410)

 -- Joshua Timberman <joshua@opscode.com>  Thu, 13 Aug 2009 12:25:59 -0600
